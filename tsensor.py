import subprocess
import time

pinNumber = 19
sensorModel = 'DHT11'
while 1:
	proc = subprocess.Popen(['dht-sensor ' + str(pinNumber) + ' ' + sensorModel], stdout=subprocess.PIPE, shell=True)
	(out, err) = proc.communicate()
	sensor_data = out.split('\n')
	humidity = sensor_data[0]
	temperature = sensor_data[1]
	if (str(humidity) != "humidity: -255.000000"):
		milli_sec = int(round(time.time() * 1000))
		print(time.strftime("%H:%M:%S"))
		aux1=int(temperature[13:-7])
		print "Temperatura:" + str(aux1) + " grados"
		aux1=int(humidity[10:-7])
		print "Humedad:"+str(aux1)+ "%"
		time.sleep(8)